const daftarMakanan = [
	{fotoMakanan: "props/menu_lengkap.jpg", namaMakanan: "Pecel Lele", hargaMakanan: "Rp.15.000"},
	{fotoMakanan: "props/nasi.jpg", namaMakanan: "Nasi", hargaMakanan: "Rp.7000"},
	{fotoMakanan: "props/pecelayam.jpg", namaMakanan: "Pecel Ayam", hargaMakanan: "Rp.15.000"},
	{fotoMakanan: "props/sambal.jpg", namaMakanan: "Sambal", hargaMakanan: "Rp.5000"},
	{fotoMakanan: "props/lalapan.jpg", namaMakanan: "Lalapan", hargaMakanan: "Rp.4000"},
	{fotoMakanan: "props/tahutempe.jpg", namaMakanan: "Tahu Tempe", hargaMakanan: "Rp.5000"},
	{fotoMakanan: "props/esteh.jpg", namaMakanan: "Es Teh Manis", hargaMakanan: "Rp.5000"},
	{fotoMakanan: "props/esjeruk.jpg", namaMakanan: "Es Jeruk", hargaMakanan: "Rp.6000"},
	{fotoMakanan: "props/pecelbebek.jpg", namaMakanan: "Bebek Goreng", hargaMakanan: "Rp.15.000"},
	{fotoMakanan: "props/airputih.jpg", namaMakanan: "Air Putih", hargaMakanan: "Rp.4000"}
];


const body = document.querySelector("#boxes");

for(let data of daftarMakanan){
	body.innerHTML += `<div class="box">
							<img src="${data.fotoMakanan}">
							<h3>${data.namaMakanan}</h3>
							<p>${data.hargaMakanan}</p>
					  </div>`;
}